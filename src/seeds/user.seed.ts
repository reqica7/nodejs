import { Factory, Seeder } from 'typeorm-seeding'
import { Connection } from 'typeorm'
import { User } from '../../src/entity/User'

import passwordHash from "./../helpers/passwordHash";

export default class CreateUsers implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    await factory(User)().createMany(10, { password : await passwordHash.hashPassword("test1234")})
  }
}