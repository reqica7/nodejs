import * as jwt from "jsonwebtoken";

import config from "../config/config";

// Create token for one day
const createJwtToken = async (payload : any) => {
  return new Promise((resolve, reject) => {
    jwt.sign(
      payload,
      config.jwtSecret,
      {
        expiresIn: 86400,
      },
      (err, token) => {
        if (err) {
          reject(err);
          return;
        }

        resolve(token);
      }
    );
  });
};

export default {
  createJwtToken
};
