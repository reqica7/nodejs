import bcrypt from "bcryptjs";

export default {
	// Hash password using bcrypt
  async hashPassword(password : string) {
		return bcrypt.hashSync(password, 10);
  },

	// Compare password using bcrypt
  async checkPassword(password : string, hash : string) {
    return bcrypt.compareSync(password, hash);
  },
};
