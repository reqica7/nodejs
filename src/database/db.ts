import { createConnection, getConnectionOptions } from 'typeorm';

export const connectDB = async (dbName : string) => {
	const connectionOptions = await getConnectionOptions(dbName);
  await createConnection({...connectionOptions, name : "default"});
};