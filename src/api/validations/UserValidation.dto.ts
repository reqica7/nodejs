import { IsNotEmpty, MinLength, MaxLength } from "class-validator";
import { Match } from "../../helpers/match";

export class UserValidation{

	@IsNotEmpty()
	username : string;

	@IsNotEmpty()
	@MinLength(6)
	@MaxLength(15)
	password : string;
}

export class RegistrationValidation extends UserValidation{

	@IsNotEmpty()
	@Match('password')
	@MinLength(6)
	@MaxLength(15)
	passwordConfirmation : string;

}

export class ChangePasswordValidation {

	@IsNotEmpty()
	@MinLength(6)
	@MaxLength(15)
	oldPassword : string;

	@IsNotEmpty()
	@MinLength(6)
	@MaxLength(15)
	password : string;

	@IsNotEmpty()
	@Match('password')
	@MinLength(6)
	@MaxLength(15)
	passwordConfirmation : string;
}