import express from "express";

import  userController  from "./../controllers/userController";
import { UserValidation, RegistrationValidation, ChangePasswordValidation }  from "../validations/UserValidation.dto";
import { validation } from "./../../middlewares/validation";
import { verifyToken } from "../../middlewares/authorization";

const router = express.Router();

router.get('/me',  [verifyToken], userController.getCurrnentUser)

router.post(
	"/signup",
	validation(RegistrationValidation),
	userController.userRegister
);

router.post(
	"/login",
	validation(UserValidation),
	userController.userLogin
);

router.put(
	"/me/update-password",
	[verifyToken],
	validation(ChangePasswordValidation),
	userController.userUpdatePassword
);

router.get(
	"/user/:id/",
	userController.getUserById
);

router.put(
	"/user/:id/like",
	[verifyToken],
	userController.likeUser
);

router.put(
	"/user/:id/unlike",
	[verifyToken],
	userController.unlikeUser
);

router.get(
	"/most-liked",
	userController.mostLikedUser
);

export default router;