import { Request, Response } from "express";

import userService from "../services/userService";

const userRegister = async (req: Request, res: Response) => {
	try {
		const checkUsername = await userService.findUser(req.body.username);

		if(checkUsername){
			return res.status(422).send("This user already exists!");
		}

		const user = await userService.userRegister(req.body);
		res.send(user);
	} catch(e){
		res.status(400).send({ message : "Bad Request!" });
	}
};

const userLogin = async (req: Request, res: Response) => {
	try {
		const user = await userService.userLogin(req.body);
		res.send(user);
	} catch(e){
		res.status(422).send({ message : "Password or Username is wrong!" });
	}
};

const getCurrnentUser = async (req: Request, res: Response) => {
	try {
		const user = await userService.getCurrentUser(res.locals.jwtPayload.username);
		res.send(user);
	} catch(e){
		res.status(401).send({ message : "Unauthorized!" });
	}
};

const userUpdatePassword = async (req: Request, res: Response) => {
	try {
		const user = await userService.findUser(res.locals.jwtPayload.username);

    // Check if old password match
    if (!user.verifyPassword(req.body.oldPassword)) {
			return res.status(422).send({ message : "Password doesnt match" });
		}

		res.send(await userService.updatePassword(user, req.body));
	} catch(e){
		res.status(400).send({ message : "Bad Request!" });
	}
};

const getUserById = async (req: Request, res: Response) => {
	try {
		const user = await userService.getUserById(req.params.id);
		res.send(user);
	} catch(e){
		res.status(400).send({ message : "Bad Request!" });
	}
};

const likeUser = async (req: Request, res: Response) => {
	try {
		const user = await userService.likeUser(req.params.id, res.locals.jwtPayload.username);
		res.send(user);
	} catch(e){
		res.status(400).send({ message : "Bad Request!" });
	}
};

const unlikeUser = async (req: Request, res: Response) => {
	try {
		const user = await userService.unlikeUser(req.params.id, res.locals.jwtPayload.username);
		res.send(user);
	} catch(e){
		res.status(400).send({ message : "Bad Request!" });
	}
};

const mostLikedUser = async (req: Request, res: Response) => {
	try {
		const users = await userService.mostLikedUser();
		res.send(users);
	} catch(e){
		res.status(400).send({ message : "Bad Request!" });
	}
};

export default  {
	userRegister,
	userLogin,
	getCurrnentUser,
	userUpdatePassword,
	getUserById,
	likeUser,
	unlikeUser,
	mostLikedUser
}