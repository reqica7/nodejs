import { getRepository } from "typeorm";

import { User } from '../../entity/User';
import passwordHash from "../../helpers/passwordHash";
import jwtHelper from "../../helpers/jwtHelper";

const userService = {
	async findUser(username : string){
		const userRepository = getRepository(User);

    try {
      return await userRepository.findOne({ where: { username } });
    } catch (error) {
      throw new Error('Something went wrong!')
		}
	},
	async getCurrentUser(username : string){
		const userRepository = getRepository(User);

		let user : User;
    try {
      user = await userRepository.findOne({ where: { username } });
    } catch (error) {
      throw new Error('Something went wrong!')
		}

		// Sing JWT, valid for 1 hour
		const token = await jwtHelper.createJwtToken({ userId: user.id, username: user.username });

		// User response object
		const userRo = user.responseObject();

    // Send the jwt in the response
    return {
				...userRo,
				token
		};
	},
	async userRegister(data : any) {

		const userRepository = getRepository(User);
		const createUser = await userRepository.create({
			...data,
		  password : await passwordHash.hashPassword(data.password)
		});

		const result = await userRepository.save(createUser);

		let user: User;
    try {
      user = await userRepository.findOneOrFail({ where: { username : data.username } });
    } catch (error) {
      throw new Error('Something went wrong!')
		}

		// Sing JWT, valid for 1 hour
		const token = await jwtHelper.createJwtToken({ userId: user.id, username: user.username });

		// User response object
		const userRo = user.responseObject();

    // Send the jwt in the response
    return {
				...userRo,
				token
		};
	},
	async userLogin(data : any) {
		const { username, password } = data;
		const userRepository = getRepository(User);

    let user: User;
    try {
      user = await userRepository.findOneOrFail({ where: { username } });
    } catch (error) {
      throw new Error('Something went wrong!')
		}

    // Check if encrypted password match
    if (!user.verifyPassword(password)) {
			throw new Error('Something went wrong!');
    }

    // Sing JWT, valid for 1 hour
		const token = await jwtHelper.createJwtToken({ userId: user.id, username: user.username });

		// User response object
		const userRo = user.responseObject();

    // Send the jwt in the response
    return {
				...userRo,
				token
		};
	},
	async updatePassword(user : User, data : any){
		const userRepository = getRepository(User);
		const { password } = data;

		userRepository.merge(user, {
			password : await passwordHash.hashPassword(password)
		});

		await userRepository.save(user);

		// Sing JWT, valid for 1 hour
		const token = await jwtHelper.createJwtToken({ userId: user.id, username: user.username });

		// User response object
		const userRo = user.responseObject();

    // Send the jwt in the response
    return {
				...userRo,
				token
		};
	},
	async getUserById(id : string){
		const userRepository = getRepository(User);

		let user: User;
    try {
       user = await userRepository.findOne({ where: { id }, relations : ['likes'] });
    } catch (error) {
      throw new Error('Something went wrong!')
		}

		return user.responseObject(true);
	},
	async likeUser(id : string, usernameOfCurrent: string){
		const userRepository = getRepository(User);

		let user: User;
		let currentUser : User;
    try {
       user = await userRepository.findOne({ where: { id }, relations : ['likes'] });
       currentUser = await userRepository.findOne({ where: { username : usernameOfCurrent } });
			 const allLikes : User[] =  user.likes;
			 userRepository.merge(user, {
				 likes : allLikes.concat(allLikes, currentUser)
			 });
			 await userRepository.save(user);
    } catch (error) {
      throw new Error('Something went wrong!')
		}

		return user.responseObject(true);
	},

	async unlikeUser(id : string,  usernameOfCurrent: string){
		const userRepository = getRepository(User);

		let user: User;
		let currentUser : User;
    try {
			 user = await userRepository.findOne({ where: { id }, relations : ['likes'] });
			 currentUser = await userRepository.findOne({ where: { username : usernameOfCurrent } });
			 user.likes = await user.likes.filter(userLike => userLike.id !== currentUser.id);
			 await userRepository.save(user);
			} catch (error) {
      throw new Error('Something went wrong!')
		}

		return user.responseObject(true);
	},
	async mostLikedUser(){
		let users : User[];
    try {
         users = await getRepository(User)
					.query(`SELECT user.*, COUNT(user_likes_user.userId_2) as likesCount
							FROM user
							LEFT JOIN user_likes_user ON user_likes_user.userId_1 = user.id
							GROUP BY user.id
							ORDER BY likesCount DESC;
					`);
    } catch (error) {
      throw new Error('Something went wrong!')
		}

		return users;
	},
}

export default userService;