import { Request, Response, NextFunction } from "express";
import * as jwt from "jsonwebtoken";

import config from "../config/config";

export const verifyToken = (req: Request, res: Response, next: NextFunction) => {

  // Get the jwt token from the head
  const token = req.headers.authorization
	? req.headers.authorization.split(" ")[1]
	: "" as string;

  let jwtPayload;

  // Try to validate the token and get data
  try {
    jwtPayload = (jwt.verify(token, config.jwtSecret) as any);
    res.locals.jwtPayload = jwtPayload;
  } catch (error) {
    // If token is not valid, respond with 401 (unauthorized)
    res.status(401).send({ message : "Unauthorized!"});
    return;
  }

  // Call the next middleware or controller
  next();
};