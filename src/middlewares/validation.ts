import { Request, Response, NextFunction } from "express";
import { validate } from "class-validator";
import { plainToClass } from "class-transformer";

export function validation (dtoClass: any) {
	return function (req: Request, res: Response, next: NextFunction) {

			const output: any = plainToClass(dtoClass, req.body);
			validate(output, { skipMissingProperties: false }).then(errors => {
					// errors is an array of validation errors
					if (errors.length > 0) {
						// tslint:disable-next-line:no-console
							console.log(errors);
							let errorTexts = Array();
							for (const errorItem of errors) {
									errorTexts = errorTexts.concat(errorItem.constraints);
							}
							res.status(422).send(errorTexts);
							return;
					} else {
							res.locals.input = output;
							next();
					}
			});
	};
};