import Faker from 'faker'
import { define } from 'typeorm-seeding'
import { User } from '../../src/entity/User'

define(User, (faker: typeof Faker) => {

	const user = new User()
	user.username = faker.internet.userName();
	user.password = "test1234";
	user.email = faker.internet.email();
	user.firstName = faker.name.firstName();
	user.lastName = faker.name.lastName();

  return user
})