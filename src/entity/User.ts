import { Entity, PrimaryGeneratedColumn, Column, ManyToMany, JoinTable } from "typeorm";
import bcrypt from "bcryptjs";

@Entity()
export class User{
	@PrimaryGeneratedColumn()
	id : number;

	@Column()
	username: string;

	@Column()
	password: string;

	@Column({nullable : true})
	firstName: string;

	@Column({nullable : true})
	lastName: string;

	@Column({nullable : true})
	email: string;

	@ManyToMany(type => User)
	@JoinTable()
	likes: User[];

	// Check if passwrod is correct
	verifyPassword(unencryptedPassword: string) {
    return bcrypt.compareSync(unencryptedPassword, this.password);
	}

	// Formating response from user entity
	responseObject(showLikesNumber = false){
		const response : any = {
			id : this.id,
			username : this.username,
			email : this.email ?? "",
			name : `${this.firstName ?? ""} ${this.lastName ?? ""}`
		};

		if(showLikesNumber){
			response.likes = this.likes.map((userLiked => userLiked.responseObject()))
			response.likesCount = this.likes.length
		}

		return response;
	}
}