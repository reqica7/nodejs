import express, { Request, Response } from "express";
import bodyParser from "body-parser";

import userRoutes from "./api/routes/users";
import { connectDB } from "./database/db";


const app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get("/", (req : Request, res : Response) => {
	res.send("Hello world");
});

app.use("/", userRoutes);

const startServer = async () => {
	await app.listen(port, () => {
		// tslint:disable-next-line:no-console
		console.log(`Server running on http://localhost:${port}`);
});
};

export default app;

(async () => {
	await connectDB(process.env.DB_ENV);
	await startServer();
})();
