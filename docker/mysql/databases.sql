# create databases
CREATE DATABASE IF NOT EXISTS `assignment`;
CREATE DATABASE IF NOT EXISTS `test_assignment`;

# create root user and grant rights
CREATE USER 'root'@'localhost' IDENTIFIED BY 'local';
GRANT ALL ON *.* TO 'root'@'%';