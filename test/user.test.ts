import request from "supertest";

import app from "../src";
import { connectDB } from "../src/database/db";

jest.useFakeTimers()

beforeAll(async () => {
	await connectDB(process.env.DB_ENV)
});

describe("Signup", () => {
	it("Signup Fail Request", async done => {
		const res = await request(app).post("/signup").send({
			"username"  : "test21_11",
			"password"  : "password",
			"firstName" : "Test"
		});
		expect(res.status).toBe(422)
		done();
	});
	
	it("Signup Request", async done => {
		const res = await request(app).post("/signup").send({
			"username"  : "user_testers",
			"password"  : "password",
			"passwordConfirmation" : "password",
			"firstName" : "Test"
		});
		expect(res.status).toBe(200)
		expect(res.body.username).toBe("user_testers");
		done();
  });
});

describe("Login", () => {
	it("Login Fail Request", async done => {
		const res = await request(app).post("/login").send({
			"username"  : "test21_11",
		});
		expect(res.status).toBe(422)
		done();
	});
	
	it("Login Request", async done => {
		const res = await request(app).post("/login").send({
			"username"  : "user_testers",
			"password"  : "password"
		});
		expect(res.status).toBe(200)
		expect(res.body.username).toBe("user_testers");
		done();
  });
});

describe("Logged in user", () => {
	it("Get logged in user", async done => {
		const loginRes = await request(app).post("/login").send({
			"username"  : "user_testers",
			"password"  : "password"
		});
		expect(loginRes.status).toBe(200);

		const res = await request(app).get("/me").set('Authorization', `Bearer ${loginRes.body.token}`);

		expect(res.status).toBe(200);
		expect(res.body.username).toBe("user_testers");
		done();
	});
	
	it("Fail update password", async done => {

		const loginRes = await request(app).post("/login").send({
			"username"  : "user_testers",
			"password"  : "password"
		});
		expect(loginRes.status).toBe(200);

		const res = await request(app).put("/me/update-password").send({
			"oldpassword"  : "password",
			"password"  : "password1",
			"passwordConfirmation"  : "password1"
		});

		expect(res.status).toBe(401)
		expect(res.body.message).toBe("Unauthorized!");
		done();
	});

	it("Update password", async done => {
			const loginRes = await request(app).post("/login").send({
				"username"  : "user_testers",
				"password"  : "password"
			});
			expect(loginRes.status).toBe(200);

			const res = await request(app).put("/me/update-password").send({
				"oldPassword"  : "password",
				"password"  : "password1",
				"passwordConfirmation"  : "password1"
			}).set('Authorization', `Bearer ${loginRes.body.token}`);
			expect(res.status).toBe(200)
			expect(res.body.username).toBe("user_testers");
			done();
	 });
	 

	it("Get user with likes", async done => {
		const loginRes = await request(app).post("/login").send({
			"username"  : "user_testers",
			"password"  : "password1"
		});
		expect(loginRes.status).toBe(200);

		const res = await request(app).get(`/user/${loginRes.body.id}`);
		expect(res.status).toBe(200)
		expect(res.body.username).toBe("user_testers");
		expect(Array.isArray(res.body.likes)).toBe(true);
		done();
 	});

	it("Like and unlike user", async done => {
		const loginRes = await request(app).post("/login").send({
			"username"  : "user_testers",
			"password"  : "password1"
		});
		expect(loginRes.status).toBe(200);

		const registerRes = await request(app).post("/signup").send({
			"username"  : "test_like_users",
			"password"  : "password",
			"passwordConfirmation" : "password",
			"firstName" : "Test"
		});
		expect(registerRes.status).toBe(200);

		const likeRes = await request(app).put(`/user/${registerRes.body.id}/like`).set('Authorization', `Bearer ${loginRes.body.token}`);
		expect(likeRes.status).toBe(200)
		expect(Array.isArray(likeRes.body.likes)).toBe(true);
		expect(typeof likeRes.body.likes.find((el : any) => el.id === loginRes.body.id)).toBe('object');

		const unlikeRes = await request(app).put(`/user/${registerRes.body.id}/unlike`).set('Authorization', `Bearer ${loginRes.body.token}`);
		expect(unlikeRes.status).toBe(200)
		expect(unlikeRes.body.likesCount).toBe(0);

		const mostLiked = await request(app).get(`/most-liked`).set('Authorization', `Bearer ${loginRes.body.token}`);
		expect(mostLiked.status).toBe(200)
		expect(Array.isArray(mostLiked.body)).toBe(true);
		expect(typeof mostLiked.body.find((el : any) => el.id === loginRes.body.id)).toBe('object');
		done();
 	});

});