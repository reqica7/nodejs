# Assignment
This is test assignment from Povio.

# Argument Database choice
I choose relational db because for myself i prefer db to be well structed and 
my data to be related to each other. Otherwise i know that nosql db are more 
flexible and can be faster but for this assignment i choose relational db because
i wanted to relate user with other user that liked him. 

# MYSQL VS POSTGRES
I choose mysql becuase i feel more comfortable with it not for a specific reason. I think
both database are great and i have good knowledge on both dbs.

# Run Docker
```sh
$ docker-compose up --build
```